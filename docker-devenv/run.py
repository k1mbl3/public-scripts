import argparse
import sys
import os
import os.path

__APP__ = 'docker-devenv'
__VERSION__ = '0.0.9'

TMPL = r"""#! /usr/bin/env python3

import random
import os
import sys
import json
import shlex

__APP__ = 'docker-devenv'
__VERSION__ = '0.0.9'

#########################
### DOCKER DATA START ###
_bool_env = lambda key, default: str(os.environ.get(key, default)).strip().lower() in ['true', '1']
SILENT_BUILD = _bool_env('SILENT_BUILD', 0) # NEEDED when using "docker-devenv" as linter's interpreter
BIN = os.environ.get('BIN', 'docker') # use "podman" if running inside a docker image
HOST_NETWORK = _bool_env('HOST_NETWORK', 0) # Enables docker's "host" network mode
RUN = _bool_env('RUN', 0) # Builds the dev-env and run "$@"

VARS_REPLACE_MAP = {
    '$ENTRYPOINT': '/__entrypoint',
    '$CODE_HOST': '0.0.0.0',
    '$CODE_PORT': '8080',
}

PORT_MAP = { # for code-server's auto-forward (ignored if HOST_NETWORK=1)
    #"HOST": "CONTAINER", # Bound port on "CONTAINER" to be open on "HOST"
    #"$RANDOM": "999999", # "HOST" can be $RANDOM
}

DOCKERFILE = f'''
FROM {{image}}
'''

ENTRYPOINT_SRC = '''
'''

ENTRYPOINT_BINARY = 'sh'
#### DOCKER DATA END ####
#########################

VALID_BIN_LIST = ['docker', 'podman']
assert BIN in VALID_BIN_LIST, f'"{BIN}" is not valid bin (must be one of: {VALID_BIN_LIST})'

def is_port_in_use(port):
    import socket
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
        return s.connect_ex(('localhost', port)) == 0

def assign_random_port(attempts=100):
    for _ in range(attempts):
        n = random.randint(1025, 65000)
        if not is_port_in_use(n):
            return n
    raise Exception(f'Failed to assign random port (attempts: {attempts})')

PORT_MAP_S = ''
for host, cont in PORT_MAP.items():
    if host == '$RANDOM':
        host = assign_random_port()
    host, cont = map(int, (host, cont))
    assert not is_port_in_use(host), f'Host\'s port {host} is already in use'
    PORT_MAP_S += f'-p {host}:{cont} '
PORT_MAP_S = PORT_MAP_S.strip()

rand_n = lambda: random.randint(0, 9999999)
tag = f'tmp-docker-devenv'
tmp_dst_path = f'/tmp/{tag}.Dockerfile'

def dump_string_to_file(src, file):
    cmds = [f'rm -f {file}']
    for i in src.strip().split('\n'):
        c = f'echo \'{i}\' >> {file}'
        cmds.append(c)
    cmds_s = ' && '.join(cmds)
    return f'RUN {cmds_s}\n'

if RUN:
    ENTRYPOINT_SRC += f'\n{ENTRYPOINT_BINARY} -c "$@"'

entrypoint_array = []
if ENTRYPOINT_SRC.strip():
    DOCKERFILE += dump_string_to_file(ENTRYPOINT_SRC, '$ENTRYPOINT')
    entrypoint_array = [ENTRYPOINT_BINARY, '$ENTRYPOINT']
    entrypoint_array = json.dumps(entrypoint_array)

PWD = os.getcwd()
DOCKERFILE += f'WORKDIR {PWD}\n'

if entrypoint_array:
    DOCKERFILE += f'ENTRYPOINT {entrypoint_array}\n'

for k, v in VARS_REPLACE_MAP.items():
    DOCKERFILE = DOCKERFILE.replace(k, v)

if os.path.isfile(tmp_dst_path):
    os.remove(tmp_dst_path)

with open(tmp_dst_path, 'w') as f:
    f.write(DOCKERFILE)

### host-pwd

import subprocess
import os
import sys

def exec_output(cmd, exitcode=False):
    if exitcode:
        return subprocess.getstatusoutput(cmd)
    return subprocess.getoutput(cmd)

def get_container_id():
    #return exec_output('hostname') # weak: depends on not changing the default hostname
    for i in open('/proc/self/mountinfo').readlines():
        if BIN == 'docker':
            if 'docker' in i and 'containers' in i:
                return i.split('docker/containers/')[1].split('/')[0].strip()
        elif BIN == 'podman':
            if 'containers' in i and 'overlay-containers' in i:
                return i.split('containers/overlay-containers/')[1].split('/')[0].strip()
        else:
            raise Exception(f'"{BIN}" is not valid bin (must be one of: {VALID_BIN_LIST})')

def get_host_pwd(path=None):
    if not path:
        path = os.getcwd()

    container_id = get_container_id()
    if not container_id:
        return path

    jdata = exec_output(f'{BIN} inspect {container_id}')
    jdata = json.loads(jdata)[0]

    mount_points = []
    for i in jdata['HostConfig']['Binds']:
        split = i.split(':')
        if len(split) == 2:
            src, dst = split
            flag = None
        elif len(split) == 3:
            src, dst, flag = split
        else:
            continue
        mount_points.append(dict(src=src, dst=dst, flag=flag))

    for i in mount_points:
        src = i['src']
        dst = i['dst']
        if path.startswith(dst):
            realpath = path.replace(dst, src)
            return f'{realpath}'

    container_abspath = exec_output(f'readlink -f "{path}"')
    container_rootfs = jdata["GraphDriver"]["Data"]["MergedDir"]

    return f'{container_rootfs}{container_abspath}'

### host-pwd

BUILD_CMD = f'''
{BIN} build {PWD} -f {tmp_dst_path} -t {tag}
'''

if SILENT_BUILD:
    exitcode, ret = exec_output(BUILD_CMD, exitcode=1)
    if exitcode > 0:
        print(ret)
        sys.exit(exitcode)
else:
    exitcode = os.system(BUILD_CMD)
    if exitcode:
        sys.exit(exitcode)

if sys.argv:
    argv = ' '.join(sys.argv[1:])
    if argv:
        argv = shlex.quote(argv)
else:
    argv = ''

import sys
if sys.stdout.isatty():
    tty_param = '-it'
else:
    tty_param = ''

# network "host" mode
HOST_NETWORK_PARAM = '--network host' if HOST_NETWORK else ''

# docker in docker
DIND_PARAM = '-v /var/run/docker.sock:/var/run/docker.sock'

RUN_CMD = f'''
{BIN} run
    --rm
    {HOST_NETWORK_PARAM} 
    -v {get_host_pwd()}:{PWD}
    {PORT_MAP_S} {DIND_PARAM}
    {tty_param}
    {tag}
    {argv}
'''.strip().split()
RUN_CMD = ' '.join(RUN_CMD)

#print(RUN_CMD)

if not SILENT_BUILD:
    print(f'\n\nEntering {__APP__} v{__VERSION__} environment...\n\n')

sys.exit(os.system(RUN_CMD))"""

parser = argparse.ArgumentParser(
    prog = f'docker-devenv {__VERSION__}',
    description = 'Creates an entire dev-env on docker in a single python3 script',
    )

parser.add_argument('--fname', required=1)
parser.add_argument('--image', default='scratch')
parser.add_argument('--force', default=False, action='store_true')

try:
    args = parser.parse_args()
except:
    sys.exit(1)

if os.path.exists(args.fname) and not args.force:
    print(f'the fname "{args.fname}" already exist, use --force to overwrite...')
    sys.exit(1)

with open(args.fname, 'w') as f:
    src = TMPL.strip().replace('{{image}}', args.image)
    f.write(src)

os.system(f'chmod +x {args.fname}')
print()
print(f'DONE! "{args.fname}" was successfully created\n')
print(f'to run it use: ./{args.fname}\n')
