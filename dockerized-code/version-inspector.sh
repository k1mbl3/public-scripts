#!/usr/bin/env sh

to_check="code-server cloudflared docker gcloud"
found=""

for app in ${to_check}; do
    if command -v $app > /dev/null
    then
        found="${found} ${app}"
    fi
done

for app in $found; do
    version="$(${app} --version)"
    if [ $app = "code-server" ]; then
        version=$(echo $version | grep -oE '(\d.\d.\d) ([a-f0-9]{40})')
    fi
    if [ $app = "gcloud" ]; then
        version="$(echo $version | sed -n '1p')"
    fi
    echo "${app}|||${version}"
done
