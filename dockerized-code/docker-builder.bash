#!/usr/bin/env bash

set -x

for var in "${!INSTALL_@}"; do
    /usr/bin/env bash -c "${!var}"
done
