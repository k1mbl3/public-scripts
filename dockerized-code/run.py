#!/usr/bin/env python3

import urllib.request
import json
import random
import os
import docker
import sys
import subprocess
import ast
import datetime
import shutil
from collections import OrderedDict

def install_and_import(package):
    import importlib
    try:
        importlib.import_module(package)
    except ImportError:
        import pip
        pip.main(['install', package])
    finally:
        globals()[package] = importlib.import_module(package)

install_and_import('prompt_toolkit')

from prompt_toolkit.shortcuts import checkboxlist_dialog, radiolist_dialog, input_dialog

### ------ prompt_toolkit_menu_dialog.py src
import functools
from prompt_toolkit.widgets import Button, Label, Dialog
from prompt_toolkit.layout.containers import WindowAlign, HSplit
from prompt_toolkit.layout import Layout, ScrollablePane
from prompt_toolkit.key_binding.key_bindings import KeyBindings, merge_key_bindings
from prompt_toolkit.key_binding.bindings.focus import focus_next, focus_previous
from prompt_toolkit.application import Application
from prompt_toolkit.key_binding.defaults import load_key_bindings
from prompt_toolkit.application.current import get_app

def _create_app(dialog, style):
    # Key bindings.
    bindings = KeyBindings()
    bindings.add("tab")(focus_next)
    bindings.add("s-tab")(focus_previous)
    bindings.add("down")(focus_next)
    bindings.add("up")(focus_previous)

    return Application(
        layout=Layout(dialog),
        key_bindings=merge_key_bindings([load_key_bindings(), bindings]),
        mouse_support=True,
        style=style,
        full_screen=True,
    )

def _return_none():
    "Button handler that returns None."
    get_app().exit()

def menu_dialog(title="", text="", options=[], style=None):
    """
    Display a dialog with a menu (given as a list of tuples).
    Return the value associated with button.
    """

    def button_handler(v):
        get_app().exit(result=v)

    options = [
        Button(text=t, handler=functools.partial(button_handler, v))
        for t, v in options
    ]

    text += ' '*10024

    children = []
    children += [Label(text=text+'\n', dont_extend_height=True)]
    children += options

    body = ScrollablePane(HSplit(
        children,
    ))

    dialog = Dialog(
        title=title,
        body=body,
        with_background=True,
    )

    return _create_app(dialog, style)
### ------ prompt_toolkit_menu_dialog.py src

__VERSION__ = '1.0.0'
DIALOG_TITLE = f'dockerized-code v{__VERSION__}'
FINAL_DOCKERFILE_NAME = 'dockerized_code.Dockerfile'

rnd = lambda: random.randint(1, 9999999)

def exec_output(cmd):
    return subprocess.getoutput(cmd)

class Docker:
    def __init__(self, dockerfile=''):
        self.dockerfile = dockerfile
        self.built = False
        self.wd = None
        self.tag = None
    
    def pull(self, tag):
        print(f'Pulling "{tag}"...\n')
        cmd = f'sudo docker pull {tag} && echo'
        os.system(cmd)
        return self
    
    def build(self, dockerfile=None, tag=None, pwd=None):
        dockerfile = dockerfile or self.dockerfile
        assert dockerfile, f'invalid dockerfile: {dockerfile}'
        pwd = pwd or os.getcwd()
        wd = f"/{pwd.split('/')[-1]}"
        dockerfile += f'\nWORKDIR {wd}\n'
        tag = tag or f'tmp-code-server-{rnd()}'
        self.dockerfile = dockerfile
        self.tag = tag
        write_path = f'/tmp/___dockerized_code_{rnd()}_{rnd()}.Dockerfile'
        with open(write_path, 'w') as f:
            f.write(dockerfile)
        print(f'Building "{tag}"...\n')
        cmd = f'sudo docker build -t {tag} . -f {write_path} && echo'
        os.system(cmd)
        self.built = True
        return self
    
    def run(self, tag=None, pwd=None, local_port=54321, delete_after=False):
        assert self.built, 'must build first'
        tag = tag or self.tag
        pwd = pwd or os.getcwd()
        wd = f"/{pwd.split('/')[-1]}"
        host_pwd = get_host_pwd(pwd)
        mount_cfg = f'-v {host_pwd}:{wd}'
        dood_cfg = '-v /var/run/docker.sock:/var/run/docker.sock:ro'
        port_cfg = f'-p {local_port}:54321'
        cmd = f'sudo docker run -it {mount_cfg} {dood_cfg} {port_cfg} {tag}'
        print(f'Running "{cmd}"...\n')
        if delete_after:
            os.system(cmd)
            return self.delete(tag)
        return os.system(cmd)
    
    def deploy(self, base, tag, dockerfile=None):
        if not self.built:
            self.pull(base).build(dockerfile=dockerfile, tag=tag)
        cmd = ''
        cmd += 'sudo docker login && '
        cmd += f'sudo docker push {tag} && echo'
        print(f'Deploying "{tag}"...\n')
        return os.system(cmd)
    
    def delete(self, tag=None):
        tag = tag or self.tag
        assert tag, f'invalid tag: {tag}'
        print(f'Deleting "{tag}"...\n')
        cmd = f'sudo docker image rm {tag} --force && echo'
        return os.system(cmd)

    def inside_run(self, inside_cmd, tag=None):
        tag = tag or self.tag
        assert tag, f'invalid tag: {tag}'
        print(f'Running "{inside_cmd}" inside "{tag}"...\n')
        cmd = f'sudo docker run -it {tag} {inside_cmd} && echo'
        return exec_output(cmd)
    
print()
print(f'[[[ dockerized-code v{__VERSION__} ]]]')
print()

def get_tags_by_repo(repo):
    url = f'https://registry.hub.docker.com/v2/repositories/{repo}/tags/?page_size=100'
    jd = urllib.request.urlopen(url).read()
    jd = json.loads(jd)
    ret = []
    for i in jd['results']:
        name = i['name']
        status = i['tag_status']
        d = dict(
            repo = repo,
            name = name,
            image = f'{repo}:{name}',
            status = status,
        )
        ret.append(d)
    return ret

def docker_tag_data(s):
    _type = 'domain' if 'http' in s.lower() else 'user'
    split = s.split('/')
    domain_or_user = '/'.join(split[:-1])
    s = s.replace(domain_or_user+'/', '')
    split = s.split(':')
    if len(split) == 2:
        img, version = split
    else:
        img = split[0]
        version = 'latest'
    domain_or_user = domain_or_user or 'library'
    return _type, domain_or_user, img, version

def get_repo_labels_by_name(name):
    t, domain_or_user, img, version = docker_tag_data(name)
    if t == 'domain':
        return 'invalid-domain', {}

    repo = f'{domain_or_user}/{img}'
    token = urllib.request.urlopen(f'https://auth.docker.io/token?service=registry.docker.io&scope=repository:{repo}:pull')
    token = json.loads(token.read())
    token = token['token']

    headers = {
        'Accept': 'application/vnd.docker.distribution.manifest.v2+json',
        'Authorization': f'Bearer {token}',
    }

    req = urllib.request.Request(f'https://registry-1.docker.io/v2/{repo}/manifests/{version}', headers=headers)
    digest = urllib.request.urlopen(req)
    digest = json.loads(digest.read())
    digest = digest['config']['digest']
    
    req = urllib.request.Request(f'https://registry-1.docker.io/v2/{repo}/blobs/{digest}', headers=headers)
    blob = urllib.request.urlopen(req)
    blob = json.loads(blob.read())

    ret = blob.get('config', {}).get('Labels') or {}
    return '', ret

def get_container_id():
    for i in open('/proc/self/cgroup').readlines():
        if 'docker' in i:
            return i.split('/')[-1].strip()

def sudo_inpect_container(id):
    cmd = f'import docker;client = docker.from_env();print(client.api.inspect_container(\'{id}\'))'
    cmd = f'sudo {sys.executable} -c "{cmd}"'
    jdata = exec_output(cmd)
    return ast.literal_eval(jdata)

def get_host_pwd(path=None):
    if not path:
        path = os.getcwd()

    container_id = get_container_id()
    if not container_id:
        print('it seems that you are not inside a container, returning current path...\n')
        return path

    #client = docker.from_env()
    jdata = sudo_inpect_container(container_id) # client.api.inspect_container(container_id)

    mount_points = []
    for i in jdata['HostConfig']['Binds']:
        split = i.split(':')
        if len(split) == 2:
            src, dst = split
            flag = None
        elif len(split) == 3:
            src, dst, flag = split
        else:
            continue
        mount_points.append(dict(src=src, dst=dst, flag=flag))

    for i in mount_points:
        src = i['src']
        dst = i['dst']
        if path.startswith(dst):
            realpath = path.replace(dst, src)
            return f'{realpath}'

    container_abspath = exec_output(f'readlink -f "{path}"')
    container_rootfs = jdata["GraphDriver"]["Data"]["MergedDir"]

    return f'{container_rootfs}{container_abspath}'

def install_code_server(base):
    if base == 'ubuntu:20.04':
        cmd = ''
        cmd += 'RUN apt-get update && apt-get install -y curl bash && '
        cmd += 'curl -fsSL https://code-server.dev/install.sh | sh && '
        cmd += 'rm -rf /var/lib/apt/lists/*'
    else:
        raise Exception(f'invalid base image: {base}')
    return cmd

def install_cloudflared(base):
    if base == 'ubuntu:20.04':
        cmd = ''
        cmd += 'RUN apt-get update && apt-get install -y wget && '
        cmd += 'wget -O /usr/bin/cloudflared https://github.com/cloudflare/cloudflared/releases/latest/download/cloudflared-linux-amd64 && '
        cmd += 'chmod +x /usr/bin/cloudflared && '
        cmd += 'rm -rf /var/lib/apt/lists/*'
    else:
        raise Exception(f'invalid base image: {base}')
    return cmd

def install_docker(base):
    if base == 'ubuntu:20.04':
        cmd = ''
        cmd += 'RUN apt-get update && apt-get install -y apt-transport-https ca-certificates curl software-properties-common && '
        cmd += 'curl -fsSL https://download.docker.com/linux/ubuntu/gpg | apt-key add - && add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu focal stable" && '
        cmd += 'apt-get update && apt-get install -y docker-ce && '
        cmd += 'rm -rf /var/lib/apt/lists/*'
    else:
        raise Exception(f'invalid base image: {base}')
    return cmd

def install_gcloud(base):
    if base == 'ubuntu:20.04':
        cmd = ''
        cmd += 'RUN apt-get update && apt-get install -y curl bash python3 && '
        cmd += 'curl -sSL https://sdk.cloud.google.com | bash -s -- --disable-prompts && '
        cmd += 'rm -rf /var/lib/apt/lists/*\n'
        cmd += 'ENV PATH $PATH:/root/google-cloud-sdk/bin'
    else:
        raise Exception(f'invalid base image: {base}')
    return cmd

def install_python3_pip(base):
    if base == 'ubuntu:20.04':
        cmd = ''
        cmd += 'RUN apt-get update && apt-get install -y python3-pip && '
        cmd += 'rm -rf /var/lib/apt/lists/*\n'
    else:
        raise Exception(f'invalid base image: {base}')
    return cmd

def install_nodejs(base):
    if base == 'ubuntu:20.04':
        cmd = ''
        cmd += 'RUN apt-get update && apt-get install -y curl bash && '
        cmd += 'curl -fsSL https://deb.nodesource.com/setup_current.x | bash && '
        cmd += 'apt-get install -y nodejs && '
        cmd += 'rm -rf /var/lib/apt/lists/*\n'
    else:
        raise Exception(f'invalid base image: {base}')
    return cmd

def install_nodejs_yarn(base):
    if base == 'ubuntu:20.04':
        cmd = ''
        cmd += 'RUN apt-get update && apt-get install -y curl bash && '
        cmd += 'curl -fsSL https://deb.nodesource.com/setup_current.x | bash && '
        cmd += 'apt-get install -y nodejs && '
        cmd += 'npm install --global yarn && '
        cmd += 'rm -rf /var/lib/apt/lists/*\n'
    else:
        raise Exception(f'invalid base image: {base}')
    return cmd

def initial_lines(base):
    # TODO: for alpine
    # apk add --no-cache --upgrade apk-tools && apk upgrade --no-cache --available
    cmd = ''
    #cmd += f'ENV _{rnd()}_ {rnd()}\n'
    if base == 'ubuntu:20.04':
        cmd += 'RUN apt-get update && apt-get -y upgrade && '
        cmd += 'rm -rf /var/lib/apt/lists/*\n'
    else:
        raise Exception(f'invalid base image: {base}')
    return cmd

def final_lines(base):
    if base == 'ubuntu:20.04':
        cmd = ''
        cmd += 'RUN apt-get update && apt-get install -y curl bash && '
        cmd += 'rm -rf /var/lib/apt/lists/*\n'
        cmd += "RUN echo 'set -e && exec $@' > /_entrypoint.bash\n"
        cmd += 'RUN echo "curl https://gitlab.com/k1mbl3/public-scripts/-/raw/main/dockerized-code/generic-entrypoint.bash?inline=false | bash" > /_init.bash\n'
        cmd += 'ENTRYPOINT ["bash", "/_entrypoint.bash"]\n'
        cmd += 'CMD ["bash", "/_init.bash"]\n'

    else:
        raise Exception(f'invalid base image: {base}')
    return cmd

def gen_img_version(found_apps):
    now = datetime.datetime.utcnow()

    def slugify(s):
        s = s.replace(':', '-')
        s = s.replace(' ', '-')
        s = s.replace('_', '-')
        return s
    
    lst = [f'{now:%Y%m%d}']
    for k, v in found_apps.items():
        k, v = map(slugify, (k, v))
        lst.append(f'{k}-{v}')
    return '_'.join(lst)[:128]

FINAL_DUMP_REWRITE_MSG = f'*** Check out your new "{FINAL_DOCKERFILE_NAME}" and rerun dockerized-code\n'

def dump_final_dockerfile(tag):
        dockerfile = ''
        dockerfile += f'FROM {tag}\n'
        with open(FINAL_DOCKERFILE_NAME, 'w') as f:
            f.write(dockerfile)
        print(FINAL_DUMP_REWRITE_MSG)
        return

def replace_base_image(src_path, img):
    def find_import_line(s):
        lines = s.split('\n')
        for ix, line in enumerate(lines):
            if 'FROM' in line:
                return True, (ix, line), lines
        return False, (None, None), lines

    content = open(src_path).read()
    backup_path = f'{src_path}.bak'
    print('Creating old Dockerfile\'s backup...\n')
    shutil.copyfile(src_path, backup_path)
    ok, (ix, line), lines = find_import_line(content)
    if not ok:
        print(f'{src_path} is not a valid Dockerfile')
        sys.exit(1)
    lines[ix] = f'FROM {img}'
    print('Rewriting Dockerfile...\n')
    with open(src_path, 'w') as f:
        f.write('\n'.join(lines))
    print(FINAL_DUMP_REWRITE_MSG)
    return

def handle_existing_img():
    default = 'k1mbl3/dockerized-code'
    text = f'List images from which repository? [{default}]'
    repo = input_dialog(
        title = DIALOG_TITLE,
        text = text,
    ).run()

    if repo is None:
        return sys.exit(0)

    if not repo:
        repo = default
    NO_TAG_FOUND = 'no_tag_found'
    tags = get_tags_by_repo(repo)
    images = [(i['image'].replace(repo+':', ''), i['image']) for i in tags]
    images.append(['Exit', NO_TAG_FOUND])
    if not images:
        images = [('### NO TAG FOUND, SELECT TO EXIT ###', NO_TAG_FOUND)]
    chosen = menu_dialog(
        title = DIALOG_TITLE,
        text = f'Select image from "{repo}"',
        options = images,
    ).run()
    if chosen == NO_TAG_FOUND:
        return sys.exit(0)
    return chosen

def handle_deploy_img():
    dockerfile = ''
    base_images = ['ubuntu:20.04']
    base = radiolist_dialog(
        title = DIALOG_TITLE,
        text = 'Select the base image',
        values = [[_]*2 for _ in base_images]
    ).run()

    if base is None:
        return sys.exit(0)

    split = base.split(':')
    if len(split) == 2:
        base_img, base_version = base.split(':')
    else:
        base_img = base
        base_version = 'latest'

    dockerfile += f'FROM {base}\n'
    #dockerfile += initial_lines(base) + '\n'
    default_apps = [
        ('code_server', install_code_server),
        ('cloudflared', install_cloudflared),
    ]
    for app, fn in default_apps:
        cmd = fn(base)
        dockerfile += cmd + '\n'

    LATEST_DOCKER = 'latest_docker'
    LATEST_GCLOUD = 'latest_gcloud'
    LATEST_PYTHON3_PIP = 'latest_python3_pip'
    LATEST_NODEJS = 'latest_nodejs'
    LATEST_NODEJS_YARN = 'latest_nodejs_yarn'

    opts_apps_map = OrderedDict([
        (LATEST_DOCKER, ('Latest Docker', install_docker)),
        (LATEST_GCLOUD, ('Latest Google Cloud SDK', install_gcloud)),
        (LATEST_PYTHON3_PIP, ('Latest Python3 + Pip3', install_python3_pip)),
        (LATEST_NODEJS, ('Latest NodeJs', install_nodejs)),
        (LATEST_NODEJS_YARN, ('Latest NodeJs + Yarn', install_nodejs_yarn)),
    ])

    choices = [
        (k, v[0]) for k, v in opts_apps_map.items()
    ]

    opts_apps = checkboxlist_dialog(
        title = DIALOG_TITLE,
        text = 'Select optional apps',
        values = choices,
    ).run()

    if opts_apps is None:
        return sys.exit(0)

    for i in opts_apps:
        fn = opts_apps_map[i][1]
        cmd = fn(base)
        dockerfile += cmd + '\n'

    # final static lines
    dockerfile += final_lines(base) + '\n'

    default = 'k1mbl3/dockerized-code'
    text = f'Deploy to which repository? [{default}]'
    repo = input_dialog(
        title = DIALOG_TITLE,
        text = text,
    ).run()
    if not repo:
        repo = default

    inpector_tag = f'dockerized-code-version-inspect-{rnd()}'
    Docker(dockerfile).pull(base).build(tag=inpector_tag)

    def version_inspect_code_server(s):
        if '[' in s:
            return s.strip().split('\n')[1].split()[0]
        return s.split()[0]

    apps_to_inspect = (
        ('code-server', version_inspect_code_server),
        ('cloudflared', lambda x: x.split()[2]),
        ('python3', lambda x: x.strip().split()[1]),
        ('node', lambda x: x.strip().split('v')[1]),
        ('yarn', lambda x: x.strip()),
        ('docker', lambda x: x.split()[2].strip(',')),
        ('gcloud', lambda x: x.split()[3]),
    )

    found = OrderedDict([(base_img, base_version)])

    for app, fn in apps_to_inspect:
        ret = Docker().inside_run(f'{app} --version', inpector_tag)
        if app in ret and 'not found' in ret.strip().lower():
            continue
        found[app] = fn(ret)

    version = gen_img_version(found)
    tag = f'{repo}:{version}'
    Docker(dockerfile).deploy(base, tag)
    Docker().delete(inpector_tag)
    return tag

def main():
    BUILD_AND_RUN = 'build_and_run'
    CHANGE_IMAGE = 'change_img'
    IGNORE = 'ignore'
    EXIT = 'exit'
    ignoring = False
    pwd_dockerfile_path = f'./{FINAL_DOCKERFILE_NAME}'
    pwd_dockerfile = os.path.isfile(pwd_dockerfile_path)
    if pwd_dockerfile:
        src = open(pwd_dockerfile_path).read()
        options = [
            ('Build and Run', BUILD_AND_RUN),
            ('Change Image', CHANGE_IMAGE),
            ('Ignore', IGNORE),
            ('Exit', EXIT),
        ]
        chosen = menu_dialog(
            title = DIALOG_TITLE,
            text = f'Found existing "{FINAL_DOCKERFILE_NAME}"',
            options=options).run()

        if chosen == EXIT:
            return sys.exit(0)

        if chosen == BUILD_AND_RUN:
            return Docker(src).build().run(delete_after=1)
        elif chosen == CHANGE_IMAGE:
            EXISTING_IMG = 'existing_img'
            CREATE_NEW_IMG = 'create_new_img'
            EXIT = 'exit'
            options = [
                ('Rewrite Dockerfile with existing image', EXISTING_IMG),
                ('Create and deploy new image then rewrite Dockerfile', CREATE_NEW_IMG),
                ('Exit', EXIT)
            ]
            
            img = menu_dialog(
                title = DIALOG_TITLE,
                text = 'How to change image?',
                options = options,
            ).run()

            if img == EXIT:
                return sys.exit(0)

            if img == EXISTING_IMG:
                chosen = handle_existing_img()
                return replace_base_image(pwd_dockerfile_path, chosen)
            elif img == CREATE_NEW_IMG:
                tag = handle_deploy_img()
                return replace_base_image(pwd_dockerfile_path, tag)
        elif chosen == IGNORE:
            ignoring = True
    
    pwd = os.getcwd()
    wd = f'{pwd.split("/")[-1]}'
    if wd[0] != '/':
        wd = '/' + wd

    EXISTING_IMG = 'existing_img'
    CREATE_NEW_IMG = 'create_new_img'

    if ignoring:
        text = f'Ignoring existing "{FINAL_DOCKERFILE_NAME}"'
    else:
        text = 'Proper Dockerfile not found'
    
    options = [
        ('Create Dockerfile with existing image', EXISTING_IMG),
        ('Create and deploy new image', CREATE_NEW_IMG),
        ('Exit', EXIT),
    ]

    img = menu_dialog(
        title = DIALOG_TITLE,
        text = text,
        options = options,
    ).run()

    if img == EXIT:
        return sys.exit(0)

    if img == EXISTING_IMG:
        chosen = handle_existing_img()
        return dump_final_dockerfile(chosen)
    else:
        tag = handle_deploy_img()
        return dump_final_dockerfile(tag)


if __name__ == '__main__':
    main()
