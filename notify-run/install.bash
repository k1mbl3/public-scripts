#!/usr/bin/env bash

if (( $EUID != 0 )); then
    echo "Please run as root"
    exit
fi

dest=/usr/bin/notify-run
wget -O $dest https://gitlab.com/k1mbl3/public-scripts/-/raw/main/notify-run/notify-run?inline=false &&\
chmod +x $dest &&\
echo DONE! installed at $dest
