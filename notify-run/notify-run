#!/usr/bin/env bash

_msg_exit() { echo $*; exit 1; }
[ -w /dev/stdout ] || _msg_exit the current user doesn\'t have write permission to /dev/stdout
[ -z "$TELEGRAM_PING_WEBHOOK" ] && _msg_exit \$TELEGRAM_PING_WEBHOOK is not set
if [ "$1" == "FAIL" ]; then shift 1; FAILONLY=1; else FAILONLY=0; fi;

_rawurlencode() { local LANG=C ; local IFS= ; while read -n1 -r -d "$(echo -n "\000")" c ; do case "$c" in [-_.~a-zA-Z0-9]) echo -n "$c" ;; *) printf '%%%02x' "'$c" ;; esac ; done }
function _fmt_dur {
    local T=$1
    local D=$((T/60/60/24))
    local H=$((T/60/60%24))
    local M=$((T/60%60))
    local S=$((T%60))
    (( $D > 0 )) && printf '%dd' $D
    (( $H > 0 )) && printf '%dh' $H
    (( $M > 0 )) && printf '%dm' $M
    printf '%ds\n' $S
}
_telegram_ping() {
    SAFE_ARGS=$(echo "$@" | _rawurlencode)
    DT=$(date +%H:%M:%S)
    HASH=$(date +%s | md5sum | awk '{print $1}')
    URL=$(printf "$TELEGRAM_PING_WEBHOOK" "$DT" "$SAFE_ARGS" "$HASH")
    wget -q $URL -O /dev/null 
}
_run() { 
    tmp="/tmp/_run_stderr_$RANDOM"
    if [ -z "$DOCKER_HOSTNAME" ]; then hostname=$(hostname); else hostname=$DOCKER_HOSTNAME; fi
    start=`date +%s`
    eval "$*" >/dev/stdout 2>$tmp
    if [ $? -eq 0 ]; then ok=1; else ok=0; fi
    end=`date +%s`
    runtime=$((end-start))
    runtime=$(_fmt_dur $runtime)
    err=$(cat $tmp)
    if [ "$err" ]; then echo $err; fi
    if [ "$ok" -eq 1 ]; then
        if [ "$FAILONLY" == "0" ]; then
            _telegram_ping "DONE [$hostname] [$PWD] [$*] [took: ${runtime}]"
        fi
    else
        _telegram_ping "FAIL [$hostname] [$PWD] [$*] [$err] [took: ${runtime}]"
    fi
    rm -f $tmp
    return 0
}
_run $*
