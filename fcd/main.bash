#!/bin/bash
set -e

VERSION="0.0.1"

# Check if fzf is installed
if ! command -v fzf &> /dev/null; then
  echo "Error: fzf is not installed. Please install fzf to use this script." >&2
  exit 1
fi

if [ -z "$1" ]; then
  src="/"
else
  src="$1"
fi

selected_dir=$(find "$src" -type d -print 2>/dev/null | fzf)

if [ -n "$selected_dir" ]; then
  cd "$selected_dir"
fi

exec $SHELL