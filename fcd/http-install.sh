src="https://gitlab.com/-/snippets/3734387/raw/main/main.bash?inline=false"
dest_path="/usr/local/bin"
dest_name="fcd"
dest="$dest_path/$dest_name"

required_commands="curl fzf"
for cmd in $required_commands; do
  if ! command -v "$cmd" > /dev/null 2>&1; then
    echo "Error: "$cmd" is not installed. Please install "$cmd" to use this script." >&2
    exit 1
  fi
done

curl -L "$src" | sudo tee "$dest" > /dev/null
sudo chmod +x "$dest"

echo
echo "fcd installed."