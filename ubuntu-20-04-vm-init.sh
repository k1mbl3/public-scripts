#!/usr/bin/bash

__VERSION__="0.0.4"

function run_or_exit() { 
    if (( $# >= 2 )); then
        id=$1
        shift
        has_name=1
    else
        id="$@"
        has_name=0
    fi
    printf "[start: $id]\n"
    if [ $has_name -eq 1 ]; then
        printf "running $@\n"
    fi
    start=`date +%s`
    eval "$@"
    if [ $? -eq 0 ]; then
        ok=1
        status="DONE"
    else
        ok=0
        status="FAIL"
    fi
    end=`date +%s`
    runtime=$((end-start))
    printf "$status\n"
    printf "[end: $id, took $runtime sec]\n\n"
    if [ $ok -eq 0 ]; then
        exit 1
    fi
}

function init() {
    sudo apt-get update
}

run_or_exit init

function install_docker() {
    sudo apt-get install -y apt-transport-https ca-certificates curl software-properties-common &&\
    curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add - &&\
    sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu focal stable" &&\
    sudo apt-get update &&\
    sudo apt-get install -y docker-ce &&\
    sudo chmod 666 /var/run/docker.sock
}

run_or_exit install_docker

function install_shiftfs() {
    sudo apt-get install -y git make dkms wget &&\
    cd /tmp &&\
    git clone -b k5.10 https://github.com/toby63/shiftfs-dkms.git shiftfs-dkms &&\
    cd shiftfs-dkms && sudo make -f Makefile.dkms &&\
    sudo modprobe shiftfs &&\
    lsmod | grep shiftfs &&\
    cd .. &&\
    rm -rf shiftfs-dkms
}

#run_or_exit install_shiftfs

run_or_exit 'sudo apt-get install -y linux-headers-$(uname -r)'

function install_sysbox() {
    DEBIAN_FRONTEND="noninteractive"
    fname="sysbox-ce_0.4.1-0.ubuntu-focal_amd64.deb"
    url="https://downloads.nestybox.com/sysbox/releases/v0.4.1/$fname"
    sudo apt-get install -y wget &&\
    cd /tmp &&\
    wget $url -O $fname &&\
    sudo apt-get install -y ./$fname &&\
    rm $fname 
}

run_or_exit install_sysbox

run_or_exit 'sudo apt-get autoremove -y && sudo apt-get clean -y'

function init_complete() {
    echo $(date) | sudo tee /__INIT_COMPLETE__ > /dev/null
}

run_or_exit init_complete
