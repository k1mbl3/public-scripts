#!/usr/bin/bash

__VERSION__="0.0.7"
__start__=`date +%s`
NONROOT="ubuntu"

function _fmt_dur {
    local T=$1
    local D=$((T/60/60/24))
    local H=$((T/60/60%24))
    local M=$((T/60%60))
    local S=$((T%60))
    (( $D > 0 )) && printf '%dd' $D
    (( $H > 0 )) && printf '%dh' $H
    (( $M > 0 )) && printf '%dm' $M
    printf '%ds\n' $S
}

function run_or_exit() { 
    if (( $# >= 2 )); then
        id=$1
        shift
        has_name=1
    else
        id="$@"
        has_name=0
    fi
    printf "[start: $id]\n"
    if [ $has_name -eq 1 ]; then
        printf "running $@\n"
    fi
    start=`date +%s`
    eval "$@"
    if [ $? -eq 0 ]; then
        ok=1
        status="DONE"
    else
        ok=0
        status="FAIL"
    fi
    end=`date +%s`
    runtime=$((end-start))
    runtime=$(_fmt_dur $runtime)
    printf "$status\n"
    printf "[end: $id, took ${runtime}]\n\n"
    if [ $ok -eq 0 ]; then
        exit 1
    fi
}

function init() {
    sudo useradd -rm -d /home/$NONROOT -s /bin/bash -g root -G sudo $NONROOT
    echo "$NONROOT:$NONROOT" | sudo chpasswd &&\
    sudo usermod -aG sudo $NONROOT &&\
    sudo fallocate -l 2G /swapfile && sudo chmod 600 /swapfile && sudo mkswap /swapfile && sudo swapon /swapfile &&\
    sudo cp /etc/fstab /etc/fstab.bak && echo '/swapfile none swap sw 0 0' | sudo tee -a /etc/fstab &&\
    sudo apt-get update && sudo apt-get upgrade -y &&\
    sudo apt-get install -y zip
}

run_or_exit init

function install_docker() {
    sudo apt-get install -y apt-transport-https ca-certificates curl software-properties-common &&\
    curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add - &&\
    sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu focal stable" &&\
    sudo apt-get update &&\
    sudo apt-get install -y docker-ce docker-compose &&\
    sudo groupadd -f docker &&\
    sudo usermod -aG docker $NONROOT
}

run_or_exit install_docker

run_or_exit 'sudo apt-get install -y linux-headers-$(uname -r)'

function install_shiftfs() {
    version=$(uname -r)
    split=(${version//./ })
    branch="k${split[0]}.${split[1]}"
    if [ $branch == "k5.8" ]; then
        branch="k5.10"
    fi
    if [ $branch == "k5.14" ]; then
        branch="k5.13"
    fi
    cd /tmp &&\
    git clone -b $branch https://github.com/toby63/shiftfs-dkms.git shiftfs-dkms
    if [ $? -ne 0 ]; then
        echo "failed to clone $branch branch, won't install shiftfs..."
        return 0
    fi
    sudo apt-get install -y git make dkms wget &&\
    cd shiftfs-dkms && ./update1 && sudo make -f Makefile.dkms &&\
    sudo modprobe shiftfs &&\
    lsmod | grep shiftfs &&\
    cd .. &&\
    rm -rf shiftfs-dkms
}

run_or_exit install_shiftfs

function install_sysbox() {
    fname="sysbox-ce_0.4.1-0.ubuntu-focal_amd64.deb"
    url="https://downloads.nestybox.com/sysbox/releases/v0.4.1/$fname"
    sudo apt-get install -y wget &&\
    cd /tmp &&\
    wget $url -O $fname &&\
    sudo DEBIAN_FRONTEND="noninteractive" apt-get install -y ./$fname &&\
    rm $fname 
}

run_or_exit install_sysbox

run_or_exit 'sudo apt-get autoremove -y && sudo apt-get clean -y'

function init_complete() {
    end=`date +%s`
    runtime=$((end-__start__))
    runtime=$(_fmt_dur $runtime)
    echo "$(date), took ${runtime}" | sudo tee /__INIT_COMPLETE__ > /dev/null
}

run_or_exit init_complete


run_or_exit 'sudo reboot'
