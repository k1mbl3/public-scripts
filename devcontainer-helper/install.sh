  #! /usr/bin/env sh

  if ! command -v sudo >/dev/null 2>&1; then
    echo "sudo is not installed on this system."
    exit 1
  fi

  if ! command -v python3 >/dev/null 2>&1; then
    echo "python3 is not installed on this system."
    exit 1
  fi

  # Check if curl is available
  if command -v curl >/dev/null 2>&1; then
    DOWNLOADER="curl"
  # Check if wget is available
  elif command -v wget >/dev/null 2>&1; then
    DOWNLOADER="wget"
  else
    echo "Neither curl nor wget is installed on this system."
    exit 1
  fi

  URL=https://gitlab.com/k1mbl3/public-scripts/-/raw/main/devcontainer-helper/main.py?inline=false
  DEST=/usr/bin/devcontainer-helper

  download() {
    if [ "$DOWNLOADER" = "curl" ]; then
      sudo sh -c "curl $URL > $DEST"
    elif [ "$DOWNLOADER" = "wget" ]; then
      sudo sh -c "wget $URL -O $DEST"
    fi
  }

  made_exec() {
    sudo chmod +x $DEST
  }

  download && made_exec
