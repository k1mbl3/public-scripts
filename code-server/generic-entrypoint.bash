#!/bin/bash

GREEN='\033[0;32m'
NC='\033[0m' # No Color
CODESERVER_PORT=12345

randint() {
    echo $(("$1" + $RANDOM % "$2"))
}

gen_passwd() {
    pw=""
    c=1
    while [ $c -le $1 ]
    do
        pw="$pw$(randint 0 9)"
        c=$(( $c + 1 ))
    done
    echo $pw
}

get_url() {
    echo $(cat $out | grep trycloudflare.com | egrep -o 'https?://[^ ]+')
}

out=/tmp/nohup.out
passwd=$(gen_passwd 6)
container_id=$(head -1 /proc/self/cgroup|cut -d/ -f3)

# populate settings.json
cfg_path="$HOME/.local/share/code-server/User"
cfg_file=$cfg_path/settings.json
mkdir -p $cfg_path && echo '{"terminal.integrated.gpuAcceleration": "canvas","workbench.colorTheme": "Default Dark+"}' > $cfg_file

loc="/init"
if [ -d "$loc" ]; then
	for i in $(ls $loc); do
		if [[ $i = [0-9]* ]]; then
			chmod +x $loc/$i
			echo "[ running $i ]"
			$loc/$i
		fi
	done
fi

touch $out
echo "updating cloudflared..."
cloudflared update
nohup cloudflared tunnel --url localhost:$CODESERVER_PORT > $out &
PASSWORD=$passwd nohup code-server --auth password --port $CODESERVER_PORT > $out &

while [[ $(get_url) = "" ]]
do
    echo "wating for public server url..."
    sleep 0.1
done

url=$(get_url)
url="$url/?folder=$PWD"

echo
echo "for logs run:"
echo ">> docker exec -it $container_id tail -f $out"
echo
echo -e "${GREEN}[URL]${NC}"
echo -e "${url}"
echo -e "${GREEN}[PASSWORD]${NC}"
echo -e "$passwd"
echo
echo "press ctrl+c to close..."
echo

tail -f $out
