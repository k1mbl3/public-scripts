###############################################################################
printf "distrobox: Installing basic packages...\n"
# Extract shell name from the $SHELL environment variable
# If not present as package in the container, we want to install it.
shell_pkg="$(basename "${SHELL:-"bash"}")"
# Ash shell is an exception, it is not a standalone package, but part of busybox.
# for this reason, use this quirk to adjust the package name to standard bash.
if [ "${shell_pkg}" = "ash" ]; then
	shell_pkg="bash"
fi

# Check dependencies in a list, and install all if one is missing
missing_packages=0
dependencies="
	bc
	bzip2
	chpasswd
	curl
	diff
	find
	findmnt
	gpg
	hostname
	less
	lsof
	man
	mount
	passwd
	pigz
	pinentry
	ping
	ps
	rsync
	script
	ssh
	sudo
	time
	tree
	umount
	unzip
	useradd
	wc
	wget
	xauth
	zip
	${shell_pkg}
"
for dep in ${dependencies}; do
	! command -v "${dep}" > /dev/null && missing_packages=1 && break
done

# Check if dependencies are met for the script to run.
if [ "${upgrade}" -ne 0 ] ||
	[ "${missing_packages}" -ne 0 ] ||
	{ [ -n "${container_additional_packages}" ] && [ ! -e /.containersetupdone ]; }; then

	# Detect the available package manager
	# install minimal dependencies needed to bootstrap the container:
	#	the same shell that's on the host
	#	sudo, mount, find
	#	passwd, groupadd and useradd
	if command -v apk; then
		# If we need to upgrade, do it and exit, no further action required.
		if [ "${upgrade}" -ne 0 ]; then
			apk upgrade
			exit
		fi
		# Check if shell_pkg is available in distro's repo. If not we
		# fall back to bash, and we set the SHELL variable to bash so
		# that it is set up correctly for the user.
		if ! apk add "${shell_pkg}"; then
			shell_pkg="bash"
		fi
		deps="
			${shell_pkg}
			alpine-base
			bash
			bash-completion
			bc
			bzip2
			coreutils
			curl
			diffutils
			docs
			findmnt
			findutils
			gcompat
			gnupg
			gpg
			iproute2
			iputils
			keyutils
			less
			libc-utils
			libcap
			lsof
			man-pages
			mandoc
			mount
			musl-utils
			ncurses
			ncurses-terminfo
			net-tools
			openssh-client
			pigz
			pinentry
			posix-libc-utils
			procps
			rsync
			shadow
			su-exec
			sudo
			tar
			tcpdump
			tree
			tzdata
			umount
			unzip
			util-linux
			util-linux-misc
			vte3
			wget
			which
			xauth
			xz
			zip
			$(apk search -q mesa-dri)
			$(apk search -q mesa-vulkan)
			vulkan-loader
		"
		install_pkg=""
		for dep in ${deps}; do
			if apk info "${dep}" > /dev/null; then
				install_pkg="${install_pkg} ${dep}"
			fi
		done
		# shellcheck disable=SC2086
		apk add --force-overwrite ${install_pkg}

		# Ensure we have tzdata installed and populated, sometimes container
		# images blank the zoneinfo directory, so we reinstall the package to
		# ensure population
		if [ ! -e /usr/share/zoneinfo/UTC ]; then
			apk del tzdata
			apk add tzdata
		fi

		# Install additional packages passed at distrbox-create time
		if [ -n "${container_additional_packages}" ]; then
			# shellcheck disable=SC2086
			apk add --force-overwrite ${container_additional_packages}
		fi

		# Workaround for when sudo is missing, we use su-exec as a replacement.
		if [ -e /sbin/su-exec ]; then
			chmod u+s /sbin/su-exec

			if [ ! -e /usr/bin/sudo ]; then
				printf "%s\n%s" '#!/bin/sh' '/sbin/su-exec root "$@"' > /usr/bin/sudo
				chmod +x /usr/bin/sudo
			fi
		fi

	elif command -v apt-get; then
		# If we need to upgrade, do it and exit, no further action required.
		if [ "${upgrade}" -ne 0 ]; then
			apt-get update
			apt-get upgrade -y
			exit
		fi
		# In Ubuntu official images, dpkg is configured to ignore locale and docs
		# This however, results in a rather poor out-of-the-box experience
		# So, let's enable them.
		rm -f /etc/dpkg/dpkg.cfg.d/excludes

		export DEBIAN_FRONTEND=noninteractive
		apt-get update
		# Check if shell_pkg is available in distro's repo. If not we
		# fall back to bash, and we set the SHELL variable to bash so
		# that it is set up correctly for the user.
		if ! apt-get install -y "${shell_pkg}"; then
			shell_pkg="bash"
		fi
		deps="
			${shell_pkg}
			apt-utils
			bash-completion
			bc
			bzip2
			curl
			dialog
			diffutils
			findutils
			gnupg
			gnupg2
			gpgsm
			hostname
			iproute2
			iputils-ping
			keyutils
			language-pack-en
			less
			libcap2-bin
			libkrb5-3
			libnss-mdns
			libnss-myhostname
			libvte-2.9*-common
			libvte-common
			locales
			lsof
			man-db
			manpages
			mtr
			ncurses-base
			openssh-client
			passwd
			pigz
			pinentry-curses
			procps
			rsync
			sudo
			tcpdump
			time
			traceroute
			tree
			tzdata
			unzip
			util-linux
			wget
			xauth
			xz-utils
			zip
			libgl1
			libegl1-mesa
			libgl1-mesa-glx
			libegl1
			libglx-mesa0
			libvulkan1
			mesa-vulkan-drivers
		"
		install_pkg=""
		for dep in ${deps}; do
			if apt-cache policy "${dep}" | grep Candidate | grep -qv none; then
				install_pkg="${install_pkg} ${dep}"
			fi
		done
		# shellcheck disable=SC2086
		apt-get install -y ${install_pkg}

		# In case the locale is not available, install it
		# will ensure we don't fallback to C.UTF-8
		if ! locale -a | grep -qi en_us.utf8; then
			sed -i "s|#.*en_US.UTF-8|en_US.UTF-8|g" /etc/locale.gen
			locale-gen
			update-locale LC_ALL=en_US.UTF-8 LANG=en_US.UTF-8
			dpkg-reconfigure locales
		fi

		# Ensure we have tzdata installed and populated, sometimes container
		# images blank the zoneinfo directory, so we reinstall the package to
		# ensure population
		if [ ! -e /usr/share/zoneinfo/UTC ]; then
			apt-get --reinstall install tzdata
		fi

		# Install additional packages passed at distrbox-create time
		if [ -n "${container_additional_packages}" ]; then
			# shellcheck disable=SC2086
			apt-get install -y ${container_additional_packages}
		fi

	elif command -v dnf; then
		# If we need to upgrade, do it and exit, no further action required.
		if [ "${upgrade}" -ne 0 ]; then
			dnf upgrade -y
			exit
		fi
		# In dnf family official images, dnf is configured to ignore locale and docs
		# This however, results in a rather poor out-of-the-box experience
		# So, let's enable them.
		sed -i '/tsflags=nodocs/d' /etc/dnf/dnf.conf

		# Check if shell_pkg is available in distro's repo. If not we
		# fall back to bash, and we set the SHELL variable to bash so
		# that it is set up correctly for the user.
		if ! dnf install -y "${shell_pkg}"; then
			shell_pkg="bash"
		fi
		deps="
			${shell_pkg}
			bash-completion
			bc
			bzip2
			curl
			diffutils
			dnf-plugins-core
			findutils
			glibc-all-langpacks
			glibc-locale-source
			gnupg2
			gnupg2-smime
			hostname
			iproute
			iputils
			keyutils
			krb5-libs
			less
			lsof
			man-db
			man-pages
			mtr
			ncurses
			nss-mdns
			openssh-clients
			pam
			passwd
			pigz
			pinentry
			procps-ng
			rsync
			shadow-utils
			sudo
			tcpdump
			time
			traceroute
			tree
			tzdata
			unzip
			util-linux
			vte-profile
			wget
			which
			whois
			words
			xorg-x11-xauth
			xz
			zip
			mesa-dri-drivers
			mesa-vulkan-drivers
			vulkan
		"
		# shellcheck disable=SC2086,2046
		dnf install -y --allowerasing $(dnf repoquery -q --latest-limit 1 --arch "$(uname -m)" ${deps})

		# In case the locale is not available, install it
		# will ensure we don't fallback to C.UTF-8
		if ! locale -a | grep -qi en_us.utf8; then
			LANG=en_US.UTF-8 localedef -i en_US -f UTF-8 en_US.UTF-8
		fi

		# Ensure we have tzdata installed and populated, sometimes container
		# images blank the zoneinfo directory, so we reinstall the package to
		# ensure population
		if [ ! -e /usr/share/zoneinfo/UTC ]; then
			dnf reinstall -y tzdata
		fi

		# Install additional packages passed at distrbox-create time
		if [ -n "${container_additional_packages}" ]; then
			# shellcheck disable=SC2086
			dnf install -y ${container_additional_packages}
		fi

	elif command -v emerge; then
		# Check if the container we are using has a ::gentoo repo defined,
		# if it is defined and it is empty, then synchroznize it.
		gentoo_repo="$(portageq get_repo_path / gentoo)"
		if [ -n "${gentoo_repo}" ] && [ ! -e "${gentoo_repo}" ]; then
			emerge-webrsync
		fi
		# If we need to upgrade, do it and exit, no further action required.
		if [ "${upgrade}" -ne 0 ]; then
			emerge --sync
			exit
		fi
		# Check if shell_pkg is available in distro's repo. If not we
		# fall back to bash, and we set the SHELL variable to bash so
		# that it is set up correctly for the user.
		if ! emerge --ask=n --autounmask-continue --noreplace --quiet-build "${shell_pkg}"; then
			shell_pkg="bash"
		fi
		deps="
			${shell_pkg}
			app-crypt/gnupg
			bash-completion
			diffutils
			findutils
			less
			ncurses
			net-misc/curl
			app-crypt/pinentry
			procps
			shadow
			sudo
			sys-devel/bc
			sys-process/lsof
			util-linux
			wget
		"
		install_pkg=""
		for dep in ${deps}; do
			if [ "$(emerge --search "${dep}" | grep "Applications found" | grep -Eo "[0-9]")" -gt 0 ]; then
				# shellcheck disable=SC2086
				install_pkg="${install_pkg} ${dep}"
			fi
		done
		# shellcheck disable=SC2086
		emerge --ask=n --autounmask-continue --noreplace --quiet-build ${install_pkg}

		# In case the locale is not available, install it
		# will ensure we don't fallback to C.UTF-8
		if ! locale -a | grep -qi en_us.utf8; then
			sed -i "s|#.*en_US.UTF-8|en_US.UTF-8|g" /etc/locale.gen
			locale-gen
			cat << EOF > /etc/env.d/02locale
LANG=en_US.UTF-8
LC_CTYPE=en_US.UTF-8
EOF
		fi

		# Install additional packages passed at distrbox-create time
		if [ -n "${container_additional_packages}" ]; then
			# shellcheck disable=SC2086
			emerge --ask=n --autounmask-continue --noreplace --quiet-build \
				${container_additional_packages}
		fi

	elif command -v microdnf; then
		# If we need to upgrade, do it and exit, no further action required.
		if [ "${upgrade}" -ne 0 ]; then
			microdnf upgrade -y
			exit
		fi
		# Check if shell_pkg is available in distro's repo. If not we
		# fall back to bash, and we set the SHELL variable to bash so
		# that it is set up correctly for the user.
		if ! microdnf install -y "${shell_pkg}"; then
			shell_pkg="bash"
		fi
		deps="
			${shell_pkg}
			bash-completion
			bc
			bzip2
			diffutils
			dnf-plugins-core
			findutils
			glibc-all-langpacks
			glibc-locale-source
			gnupg2
			gnupg2-smime
			hostname
			iproute
			iputils
			keyutils
			krb5-libs
			less
			lsof
			man-db
			man-pages
			mtr
			ncurses
			nss-mdns
			openssh-clients
			pam
			passwd
			pigz
			pinentry
			procps-ng
			rsync
			shadow-utils
			sudo
			tcpdump
			time
			traceroute
			tree
			tzdata
			unzip
			util-linux
			vte-profile
			wget
			which
			whois
			words
			xorg-x11-xauth
			xz
			zip
			mesa-dri-drivers
			mesa-vulkan-drivers
			vulkan
		"
		install_pkg=""
		for dep in ${deps}; do
			if [ "$(microdnf repoquery "${dep}" | wc -l)" -gt 0 ]; then
				install_pkg="${install_pkg} ${dep}"
			fi
		done
		# shellcheck disable=SC2086,SC2046
		microdnf install -y ${install_pkg}

		# In case the locale is not available, install it
		# will ensure we don't fallback to C.UTF-8
		if ! locale -a | grep -qi en_us.utf8; then
			LANG=en_US.UTF-8 localedef -i en_US -f UTF-8 en_US.UTF-8
		fi

		# Ensure we have tzdata installed and populated, sometimes container
		# images blank the zoneinfo directory, so we reinstall the package to
		# ensure population
		if [ ! -e /usr/share/zoneinfo/UTC ]; then
			microdnf reinstall -y tzdata
		fi

		# Install additional packages passed at distrbox-create time
		if [ -n "${container_additional_packages}" ]; then
			# shellcheck disable=SC2086
			microdnf install -y ${container_additional_packages}
		fi

	elif command -v pacman; then
		# Update the package repository cache exactly once before installing packages.
		pacman -S -y -y

		# If we need to upgrade, do it and exit, no further action required.
		if [ "${upgrade}" -ne 0 ]; then
			pacman -S -u --noconfirm
			exit
		fi
		# In archlinux official images, pacman is configured to ignore locale and docs
		# This however, results in a rather poor out-of-the-box experience
		# So, let's enable them.
		sed -i "s|NoExtract.*||g" /etc/pacman.conf
		sed -i "s|NoProgressBar.*||g" /etc/pacman.conf

		# In case the locale is not available, install it
		# will ensure we don't fallback to C.UTF-8
		if ! locale -a | grep -qi en_us.utf8; then
			sed -i "s|#.*en_US.UTF-8|en_US.UTF-8|g" /etc/locale.gen
			locale-gen -a
		fi

		# Check if shell_pkg is available in distro's repo. If not we
		# fall back to bash, and we set the SHELL variable to bash so
		# that it is set up correctly for the user.
		if ! pacman -S --needed --noconfirm "${shell_pkg}"; then
			shell_pkg="bash"
		fi
		deps="
			${shell_pkg}
			bash-completion
			bc
			curl
			diffutils
			findutils
			glibc
			gnupg
			inetutils
			keyutils
			less
			lsof
			man-db
			man-pages
			mlocate
			mtr
			ncurses
			nss-mdns
			openssh
			pigz
			pinentry
			procps-ng
			rsync
			shadow
			sudo
			tcpdump
			time
			traceroute
			tree
			tzdata
			unzip
			util-linux
			util-linux-libs
			vte-common
			wget
			words
			xorg-xauth
			zip
			mesa
			opengl-driver
			vulkan-intel
			vte-common
			vulkan-radeon
		"
		install_pkg=""
		for dep in ${deps}; do
			if pacman -Ss "${dep}" > /dev/null; then
				install_pkg="${install_pkg} ${dep}"
			fi
		done
		# shellcheck disable=SC2086
		pacman -S --noconfirm ${install_pkg}

		# Ensure we have tzdata installed and populated, sometimes container
		# images blank the zoneinfo directory, so we reinstall the package to
		# ensure population
		if [ ! -e /usr/share/zoneinfo/UTC ]; then
			pacman -S --noconfirm tzdata
		fi

		# Install additional packages passed at distrbox-create time
		if [ -n "${container_additional_packages}" ]; then
			# shellcheck disable=SC2086
			pacman -S --needed --noconfirm ${container_additional_packages}
		fi

	elif command -v slackpkg; then
		# If we need to upgrade, do it and exit, no further action required.
		if [ "${upgrade}" -ne 0 ]; then
			yes | slackpkg upgrade-all -default_answer=yes -batch=yes
			exit
		fi
		slackpkg update
		# Check if shell_pkg is available in distro's repo. If not we
		# fall back to bash, and we set the SHELL variable to bash so
		# that it is set up correctly for the user.
		if ! yes | slackpkg install -default_answer=yes -batch=yes "${shell_pkg}"; then
			shell_pkg="bash"
		fi
		deps="
			${shell_pkg}
			bash-completion
			bc
			curl
			diffutils
			findutils
			glew
			glibc
			glu
			gnupg2
			iputils
			less
			libX11
			libXau
			libXdamage
			libXdmcp
			libXext
			libXfixes
			libXxf86vm
			libdrm
			libvte-2
			libxcb
			libxcb-dri2
			libxcb-dri3
			libxcb-glx
			libxcb-present
			libxcb-randr
			libxcb-render
			libxcb-shape
			libxcb-sync
			libxcb-xfixes
			libxshmfence
			lsof
			man
			mesa
			ncurses
			openssh
			pinentry
			procps
			rsync
			shadow
			ssh
			sudo
			time
			wget
			xauth
		"
		install_pkg=""
		dep=""
		for dep in ${deps}; do
			if ! slackpkg search "${dep}" | grep -q "No package name matches the pattern"; then
				install_pkg="${install_pkg} ${dep}"
			fi
		done

		rm -f /var/lock/slackpkg.*

		# shellcheck disable=SC2086
		yes | slackpkg install -default_answer=yes -batch=yes ${install_pkg}

		# Install additional packages passed at distrbox-create time
		if [ -n "${container_additional_packages}" ]; then
			# shellcheck disable=SC2086
			yes | slackpkg install -default_answer=yes -batch=yes \
				${container_additional_packages}
		fi

	elif command -v swupd; then
		# If we need to upgrade, do it and exit, no further action required.
		if [ "${upgrade}" -ne 0 ]; then
			swupd update
			exit
		fi

		swupd bundle-add os-core-search

		deps="
			bc
			cryptography
			curl
			network-basic
			procps-ng
			rsync
			shells
			sysadmin-basic
			unzip
			wget
			x11-tools
			zip
			devpkg-Vulkan-Loader
			lib-opengl
		"
		install_pkg=""
		for dep in ${deps}; do
			if swupd search "${dep}" > /dev/null; then
				install_pkg="${install_pkg} ${dep}"
			fi
		done
		# shellcheck disable=SC2086
		swupd bundle-add ${install_pkg}

		# Install additional packages passed at distrbox-create time
		if [ -n "${container_additional_packages}" ]; then
			# shellcheck disable=SC2086
			swupd bundle-add ${container_additional_packages}
		fi

	elif command -v xbps-install; then
		# If we need to upgrade, do it and exit, no further action required.
		if [ "${upgrade}" -ne 0 ]; then
			xbps-install -Syu
			exit
		fi
		# Ensure we avoid errors by keeping xbps updated
		xbps-install -Syu xbps
		# We have to lock this package, as it's incompatible with distrobox's
		# mount process.
		xbps-pkgdb -m repolock base-files
		# Check if shell_pkg is available in distro's repo. If not we
		# fall back to bash, and we set the SHELL variable to bash so
		# that it is set up correctly for the user.
		if ! xbps-install -Sy "${shell_pkg}"; then
			shell_pkg="bash"
		fi
		deps="
			${shell_pkg}
			bash-completion
			bc
			bzip2
			curl
			diffutils
			findutils
			gnupg2
			inetutils-ping
			iproute2
			less
			lsof
			man-db
			mit-krb5-client
			mit-krb5-libs
			mtr
			ncurses-base
			nss
			openssh
			pigz
			pinentry-tty
			procps-ng
			rsync
			shadow
			sudo
			time
			traceroute
			tree
			tzdata
			unzip
			util-linux
			xauth
			xz
			zip
			wget
			vte3
			mesa-dri
			vulkan-loader
			mesa-vulkan-intel
			mesa-vulkan-radeon
		"
		install_pkg=""
		for dep in ${deps}; do
			if [ "$(xbps-query -Rs "${dep}" | wc -l)" -gt 0 ]; then
				install_pkg="${install_pkg} ${dep}"
			fi
		done
		# shellcheck disable=SC2086
		xbps-install -Sy ${install_pkg}

		# In case the locale is not available, install it
		# will ensure we don't fallback to C.UTF-8
		if ! locale -a | grep -qi en_us.utf8; then
			sed -i "s|#.*en_US.UTF-8|en_US.UTF-8|g" /etc/default/libc-locales
			xbps-reconfigure --force glibc-locales
		fi

		# Ensure we have tzdata installed and populated, sometimes container
		# images blank the zoneinfo directory, so we reinstall the package to
		# ensure population
		if [ ! -e /usr/share/zoneinfo/UTC ]; then
			xbps-install --force -y tzdata
		fi

		# Install additional packages passed at distrbox-create time
		if [ -n "${container_additional_packages}" ]; then
			# shellcheck disable=SC2086
			xbps-install -Sy ${container_additional_packages}
		fi

	elif command -v yum; then
		# If we need to upgrade, do it and exit, no further action required.
		if [ "${upgrade}" -ne 0 ]; then
			yum upgrade -y
			exit
		fi
		# In yum family official images, yum is configured to ignore locale and docs
		# This however, results in a rather poor out-of-the-box experience
		# So, let's enable them.
		sed -i '/tsflags=nodocs/d' /etc/yum.conf

		# Check if shell_pkg is available in distro's repo. If not we
		# fall back to bash, and we set the SHELL variable to bash so
		# that it is set up correctly for the user.
		if ! yum install -y "${shell_pkg}"; then
			shell_pkg="bash"
		fi
		deps="
			${shell_pkg}
			bash-completion
			bc
			bzip2
			curl
			diffutils
			dnf-plugins-core
			findutils
			glibc-all-langpacks
			glibc-locale-source
			gnupg2
			gnupg2-smime
			hostname
			iproute
			iputils
			keyutils
			krb5-libs
			less
			lsof
			man-db
			man-pages
			mtr
			ncurses
			nss-mdns
			openssh-clients
			pam
			passwd
			pigz
			pinentry
			procps-ng
			rsync
			shadow-utils
			sudo
			tcpdump
			time
			traceroute
			tree
			tzdata
			unzip
			util-linux
			vte-profile
			wget
			which
			whois
			words
			xorg-x11-xauth
			xz
			zip
			mesa-dri-drivers
			mesa-vulkan-drivers
			vulkan
		"
		# shellcheck disable=SC2086,SC2046
		yum install -y $(yum list -q ${deps} |
			grep -v "Packages" |
			grep "$(uname -m)" |
			cut -d' ' -f1)

		# In case the locale is not available, install it
		# will ensure we don't fallback to C.UTF-8
		if ! locale -a | grep -qi en_us.utf8; then
			LANG=en_US.UTF-8 localedef -i en_US -f UTF-8 en_US.UTF-8
		fi

		# Ensure we have tzdata installed and populated, sometimes container
		# images blank the zoneinfo directory, so we reinstall the package to
		# ensure population
		if [ ! -e /usr/share/zoneinfo/UTC ]; then
			yum reinstall -y tzdata
		fi

		# Install additional packages passed at distrbox-create time
		if [ -n "${container_additional_packages}" ]; then
			# shellcheck disable=SC2086
			yum install -y ${container_additional_packages}
		fi

	elif command -v zypper; then
		# If we need to upgrade, do it and exit, no further action required.
		if [ "${upgrade}" -ne 0 ]; then
			zypper dup -y
			exit
		fi
		if ! zypper install -y "${shell_pkg}"; then
			shell_pkg="bash"
		fi

		# In openSUSE official images, zypper is configured to ignore recommended
		# packages (i.e., weak dependencies). This however, results in a rather
		# poor out-of-the-box experience (e.g., when trying to run GUI apps).
		# So, let's enable them. For the same reason, we make sure we install
		# docs.
		sed -i 's/.*solver.onlyRequires.*/solver.onlyRequires = false/g' /etc/zypp/zypp.conf
		sed -i 's/.*rpm.install.excludedocs.*/rpm.install.excludedocs = no/g' /etc/zypp/zypp.conf
		# With recommended packages, something might try to pull in
		# parallel-printer-support which can't be installed in rootless containers.
		# Since we very much likely never need it, just lock it
		zypper al parallel-printer-support
		# Check if shell_pkg is available in distro's repo. If not we
		# fall back to bash, and we set the SHELL variable to bash so
		# that it is set up correctly for the user.
		deps="
			${shell_pkg}
			bash-completion
			bc
			bzip2
			curl
			diffutils
			findutils
			glibc-locale
			glibc-locale-base
			gnupg
			hostname
			iputils
			keyutils
			less
			libvte-2*
			lsof
			man
			man-pages
			mtr
			ncurses
			nss-mdns
			openssh-clients
			pam
			pam-extra
			pigz
			pinentry
			procps
			rsync
			shadow
			sudo
			system-group-wheel
			systemd
			time
			timezone
			tree
			unzip
			util-linux
			util-linux-systemd
			wget
			words
			xauth
			zip
			Mesa-dri
			libvulkan1
			libvulkan_intel
			libvulkan_radeon
		"
		# Mark gpg errors (exit code 106) as non-fatal, but don't pull anything from unverified repos
		# shellcheck disable=SC2086,SC2046
		zypper -n install -y $(zypper -n -q se -x ${deps} | grep -e 'package$' | cut -d'|' -f2) || [ ${?} = 106 ]

		# In case the locale is not available, install it
		# will ensure we don't fallback to C.UTF-8
		if ! locale -a | grep -qi en_us.utf8; then
			LANG=en_US.UTF-8 localedef -i en_US -f UTF-8 en_US.UTF-8
		fi

		# Ensure we have tzdata installed and populated, sometimes container
		# images blank the zoneinfo directory, so we reinstall the package to
		# ensure population
		if [ ! -e /usr/share/zoneinfo/UTC ]; then
			zypper install -f -y timezone
		fi

		# Install additional packages passed at distrbox-create time
		if [ -n "${container_additional_packages}" ]; then
			# shellcheck disable=SC2086
			zypper install -y ${container_additional_packages}
		fi
	else
		printf "Error: could not find a supported package manager.\n"
		printf "Error: could not set up base dependencies.\n"
		# Exit as command not found
		exit 127
	fi

	touch /.containersetupdone
fi
###############################################################################
